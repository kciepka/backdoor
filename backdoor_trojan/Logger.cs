﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace backdoor_trojan
{
    public static class Logger
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger
        (MethodBase.GetCurrentMethod().DeclaringType);

    }
}
